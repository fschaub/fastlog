module Main where

import System.IO

import FastLog
import qualified Options.Applicative as Opt
import Conduit
import qualified Data.Text as T

-- |commandline arguments
data Args = Args
  { keys :: String
  , format :: String
  , outfile :: String
  , infile :: String
  }


-- |default behaviour
defaultKeys, defaultFormat, defaultOutFile :: String
defaultKeys = "date,time,devname,devid,level,tunnelip,tunneltype,remip,user,group,dst_host,msg"
defaultFormat = "plain"
defaultOutFile = "/dev/stdout"

-- |commandline argument parser
args :: Opt.Parser Args
args = Args
  <$> (
    Opt.strOption (
      Opt.long "key"
      <> Opt.short 'k'
      <> Opt.metavar "KEY1,KEY2.KEY3"
      <> Opt.help "a list of keys to be printed"
      <> Opt.value defaultKeys
      )
  ) <*> (
    Opt.strOption (
      Opt.long "format"
      <> Opt.short 'f'
      <> Opt.metavar "(plain|csv)"
      <> Opt.help "output format"
      <> Opt.value defaultFormat
      )
  ) <*> (
    Opt.strOption (
      Opt.long "output-file"
      <> Opt.short 'o'
      <> Opt.metavar "OUTFILE"
      <> Opt.help "output file"
      <> Opt.value defaultOutFile
      )
  ) <*> (
    Opt.strArgument (
      Opt.metavar "INFILE"
      <> Opt.help "input file"
      )
  )

-- |optparse info provides parser and help text
pInfo :: Opt.ParserInfo Args
pInfo = Opt.info (args Opt.<**> Opt.helper) Opt.fullDesc


-- |run fastlog with given args
runWithArgs :: Args -> IO ()
runWithArgs args = do
  let
    inp = infile args
    outp = outfile args
    form = format args
    ks = keys args
  -- print running configuration
  putStrLn
    $     " infile: " ++ inp
    ++  "\noutfile: " ++ outp
    ++  "\n format: " ++ form
    ++  "\n   keys: " ++ ks
  runConduitRes
    $  sourceFile (infile args)
    .| decodeUtf8LenientC
    .| mapC T.unpack
    .| mapC gen
    .| mapC T.pack
    .| encodeUtf8C
    .| sinkFile (outfile args)
  -- withIOStream $ fastlog (gen instream
  where
    -- |run fastlog on fastlog instance
    gen :: String -> String
    gen = fastlog (mkFL getKeys)
    -- |get a list of keys
    getKeys :: [String]
    getKeys = words . map (\x -> if x == ',' then ' ' else x) $ keys args
    -- |make a fastlog instance from the provided format
    mkFL :: [String] -> FastLog
    mkFL =
      case format args of
        "plain" -> plainFromKeys
        "csv" -> csvFromKeys
        _ -> error $ "error: format \"" ++ format args ++ "\" not known"
    

-- |run fastlog
main :: IO ()
main = Opt.execParser pInfo >>= runWithArgs
