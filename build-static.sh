#!bin/bash

cp package.yaml package.yaml.orig && mv package-static.yaml package.yaml && echo "changing ld.gold to ld" && sudo mv /usr/bin/ld.gold /usr/bin/ld.gold.real && sudo ln -s /usr/bin/ld /usr/bin/ld.gold && echo "building" && stack clean && stack build && stack install && sudo rm /usr/bin/ld.gold && sudo mv /usr/bin/ld.gold.real /usr/bin/ld.gold && mv package.yaml package-static.yaml && mv package.yaml.orig package.yaml && mv `which fastlog` .

