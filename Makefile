all:
	stack build

static:
	bash build-static.sh

clean:
	stack clean

install:
	stack install


