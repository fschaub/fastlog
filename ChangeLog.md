# Changelog for fastlog

## Released changes

### Version v0.1.0.1

- Fixed memory usage with big files, by enabling stream processing

## Unreleased changes
