module FastLog
    ( fastlog
    , FastLog(..)
    , csvFromKeys
    , plainFromKeys
    ) where

import Text.Parsec
import Text.Parsec.Char
import Text.Parsec.Prim
import qualified Data.Map as Map

import Control.Monad
import Control.Arrow
import Data.Maybe

-- |run on a fastlog instance and an input string
fastlog :: FastLog -> String -> String
fastlog = generate


-- |class types that implement String -> String morphisms
class Generator a where
  generate :: a -> String -> String

{- | The Fastlog Data type

a generator is a string of the format "HEADER|DELIMITER|FOOTER|ENTRY".
FastLog implements a generator as a haskell datatype

the entry string fixes a format for the entries. The markup '%(foo)' will
rewrite to the value of foo in the original log:
  input: 'foo=val1 bar=val2 baz'
  generator: '|||fooelement=%(foo) barelement=%(bar)'
  output: 'fooelement=val1 barelement=val3'
if a key is not found the value will be 'NF'
  input: 'foo=val1 bar=val2 baz'
  generator: '|||fooelement=%(foo) bazelement=%(baz)'
  output: 'fooelement=val1 bazelement=NF'
 -}
data FastLog = FastLog
  { header      :: String   -- ^the HEADER section
  , separator   :: String   -- ^the DELIMITER section
  , footer      :: String   -- ^the FOOTER section
  , entry       :: String   -- ^the ENTRY section
  , replaceReserved :: String -> String   -- ^replacement function for reserved strings
  , defaultVal :: String
  }

-- |generator instance for FastLog
instance Generator FastLog where
  generate gen inp = genheader ++ genentries ++ genfooter where
    sep = separator gen
    genentries = concatWith sep . map genentry . lines $ inp
    genentry = generate (fromFastLog gen :: EntryGenerator)
    genheader = header gen
    genfooter = footer gen

defaultFastLog :: FastLog
defaultFastLog = FastLog "" "" "" "" id ""

concatWith :: [a] -> [[a]] -> [a]
concatWith s [] = []
concatWith s (x:[]) = x
concatWith s (x:xs) = x ++ concatMap (s++) xs


-- |Morphism defined by the entrysection of a Generator
newtype EntryGenerator = EntryGenerator (String -> String)
instance Generator EntryGenerator where
  generate (EntryGenerator f) = f

-- |obtain an EntryGenerator from a FastLog instance
fromFastLog :: FastLog -> EntryGenerator
fromFastLog gen =
  let
    e = entry gen
    r = replaceReserved gen . fromMaybe (defaultVal gen)
    -- Either EntryGenerator for e
    efE = parse (parseEntryFun r) "" e
    -- Either KV-Map for a string s
    kvE s = parse parseKVs "" s
    -- EntryGenerator function :: String -> Either a String
    fE s = (entryInstance <$> efE) <*> kvE s
  in
    -- return the EntryGenerator, that either fails with an error on
    -- a parse error or with the generated entry
    EntryGenerator $ either (error . show) id . fE



-- |String parser alias
type Parser a c = Parsec a () c

-- |parser instance that parses "Key=Value" pairs into a Map
parseKVs :: Parser String (Map.Map String String)
parseKVs = Map.fromList <$> parseKV where
  -- parse into a list of KV-Pairs
  parseKV = try end <|> try kvpair <|> garbage
  -- parse a KV-Pair
  kvpair = do
    kv <- do  -- the KV-Pair
      spaces
      key <- many1 (noneOf "= \n") <?> "cannot read key"
      _ <- string "=" <?> error "no kv pair found"
      val <- value <?> "cannot read value"
      return (key, val)
    (kv:) <$>  parseKV
  -- parse the EOF and return
  end = spaces >> eof >> return []
  -- consume garbage (things that are not KV-Pairs)
  garbage = many (noneOf " \n") >> spaces >> many (string "\n") >> parseKV
  -- parse a value
  value = try stringVal <|> simpleVal
  -- parse a string value (starting with " ending with ")
  stringVal = do
    op <- string "\""
    cont <- many (noneOf "\"")
    return $ op ++ cont ++ "\""
  -- parse a simple value (not containing spaces or newlines or tabs)
  simpleVal = many (noneOf " \n\t")

-- |type alias for the entry function, i.e construct an entry from a KV-Map
-- (is actually a list of functions, one function per key in the entry string)
type EntryFunction = [Map.Map String String -> String]
-- | get the entry instance from an EntryFunction and a KV-Map
entryInstance :: EntryFunction -> Map.Map String String -> String
entryInstance fs m = concatMap ($ m) fs

-- |parser for entry functins
parseEntryFun :: (Maybe String -> String) -> Parser String EntryFunction
parseEntryFun sane = try withToken <|> suff where
  -- parse for a markup token
  withToken = do
    -- the prefix of the token
    pref <- manyTill anyChar (string "%(")
    -- the token (is key of KV-Map)
    tok <- manyTill anyChar (string ")")
    -- function to replace the markup with the value (or NF)
    -- f :: Map.Map String String -> String
    let f = \m -> pref ++ sane (m Map.!? tok)
    -- recur and prepend function f
    (f :) <$> parseEntryFun sane
  -- parse the suffix and return with List containing constant suffix function
  suff = (:[]) . const <$> many anyChar



-- |generate a FastLog instance for CSVs
csvFromKeys :: [String] -> FastLog
csvFromKeys keys =
  let
    csvsep = ","
    sep = "\n"
  in defaultFastLog
    { header = concatWith csvsep keys ++ sep
    , separator = sep
    , footer = sep
    , entry = concatWith csvsep $ map (\x -> "%(" ++ x ++ ")") keys
    }

-- | generate a Fastlog instance for Plain logging
plainFromKeys :: [String] -> FastLog
plainFromKeys keys =
  let
    sep = "\n"
  in defaultFastLog
    { header = ""
    , separator = sep
    , footer = sep
    , entry = concatWith " " $ map (\x -> x ++ "=%(" ++ x ++ ")") keys
    , defaultVal = "NF"
    }


















  --
