# fastlog

prettyfy and convert Fortigate Logs

## Contents

this repo contains a bash script __prettylog__ and a statically linked ELF-executable __fastlog__. Due to the bash script being super slow and has strange behaviour with quotations, I strongly recommend using __fastlog__.

## Usage

```
$ ./fastlog --help      
Usage: fastlog [-k|--key KEY1,KEY2.KEY3] [-f|--format (plain|csv)] 
               [-o|--output-file OUTFILE] INFILE

Available options:
  -k,--key KEY1,KEY2.KEY3  a list of keys to be printed
  -f,--format (plain|csv)  output format
  -o,--output-file OUTFILE output file
  INFILE                   input file
  -h,--help                Show this help text
```

```
$ bash prettylog -h
prettylog: prettyprinter for Fortigate log files

Usage: prettylog (-h | --help)
     | prettylog (-k | --key Key1,Key2,Key3 -f | --format FORMAT) (-o | --output-file OUTPUTFILE) LOGFILE

  -k | --key            list of keys which values should be printed 
  -f | --format         oneof csv, plain; plain if none given
  -o | --output-file    the file the pretty log is written to    
                        stdout if none given
  LOGFILE               the logfile (stdin if none given)
  
  -h | --help           print this help text

```


## Default Behaviour

### prettylog

To set the default behaviour, set the corresponding Variables:

```bash
# default arguments
KEYS='date,time,devname,devid,level,tunnelip,tunneltype,remip,user,group,dst_host,msg'
FORMAT='plain'
OUTPUT='/dev/stdout'
```


### fastlog

To set the default behaviour, set the corresponding Variables located in ./app/Main.hs:

```haskell
defaultKeys, defaultFormat, defaultOutFile :: String
defaultKeys = "date,time,devname,devid,level,tunnelip,tunneltype,remip,user,group,dst_host,msg"
defaultFormat = "plain"
defaultOutFile = "/dev/stdout"
```

After that you have to recompile.


## Build fastlog

__fastlog__ uses the haskell __stack__ build system. The repository comes with a Makefile with 4 Targets: all, static, clean and install.


*   all: builds a dynamically linked executable.
*   static: builds a statically linked executable and copies it to the repository folder. Due to a Bug in _ld.gold_, this target swaps _ld.gold_ with _ld_ for the compilation and needs root privileges.
*   clean: cleans the stack working directores
*   install: copies the __dynamically linked executable__ to ~/.local/bin



If you are not interrested in modifying fastboot, just copy the statically linked executable to /usr/local/bin (or another location in your $PATH). If you build it locally and don't want to recompile when your glibc version changes, also generate a statically linked executable.
